package com.daioware.system.os;

public class OperativeSystem {

	public static enum Type{
		WINDOWS,
		MAC,
		UNIX,
		SOLARIS
	}
	
	private Type type;
	private String name;
	private String version;
	
	
	public OperativeSystem() {
		super();
	}
	public OperativeSystem(Type type, String name, String version) {
		this();
		setType(type);
		setName(name);
		setVersion(version);
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperativeSystem [type=").append(getType()).append(", name=").append(getName()).append(", version=")
				.append(getVersion()).append("]");
		return builder.toString();
	}
	
	
	
}
