package com.daioware.system.os;

public class UnsupportedOperativeSystemException extends Exception{

	private static final long serialVersionUID = 1L;

	public UnsupportedOperativeSystemException() {
		super();
	}

	public UnsupportedOperativeSystemException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnsupportedOperativeSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnsupportedOperativeSystemException(String message) {
		super(message);
	}

	public UnsupportedOperativeSystemException(Throwable cause) {
		super(cause);
	}

}
