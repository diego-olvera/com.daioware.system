package com.daioware.system.os;

public class OperativeSystemUtilities {

	public static OperativeSystem getOs() {
		OperativeSystem os=new OperativeSystem();
		String osName=System.getProperty("os.name").toLowerCase();
		String osVersion;
		int spaceIndex=osName.indexOf(" ");
		if(spaceIndex>=0) {
			osVersion=osName.substring(spaceIndex+1);
			osName=osName.substring(0,spaceIndex);
		}
		else {
			osVersion=null;
		}
		os.setName(osName);
		os.setVersion(osVersion);
		if(isWindows(osName)) {
			os.setType(OperativeSystem.Type.WINDOWS);
		}
		else if(isMac(osName)) {
			os.setType(OperativeSystem.Type.MAC);
		}
		else if(isUnix(osName)) {
			os.setType(OperativeSystem.Type.UNIX);
		}
		else if(isSolaris(osName)) {
			os.setType(OperativeSystem.Type.SOLARIS);
		}
		return os;
	}
	private static boolean isWindows(String osName) {
		return (osName.indexOf("win") >= 0);

	}
	private static boolean isMac(String osName) {
		return (osName.indexOf("mac") >= 0);

	}
	private static boolean isUnix(String osName) {

		return (osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0 );
		
	}
	private static boolean isSolaris(String osName) {
		return (osName.indexOf("sunos") >= 0);

	}

}
