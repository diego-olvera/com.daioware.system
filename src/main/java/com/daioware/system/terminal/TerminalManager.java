package com.daioware.system.terminal;

import com.daioware.system.os.OperativeSystem;
import com.daioware.system.os.OperativeSystemUtilities;
import com.daioware.system.os.UnsupportedOperativeSystemException;

public class TerminalManager {

	public static Terminal getTerminal(OperativeSystem os) throws UnsupportedOperativeSystemException {
		switch(os.getType()) {
			case MAC:return new MacTerminal(os);
			case SOLARIS:return new SolarisTerminal(os);
			case UNIX:return new UnixTerminal(os);
			case WINDOWS:return new WindowsTerminal(os);
			default:throw new UnsupportedOperativeSystemException(os.toString());
		}
	}
	
	public static Terminal getTerminal() throws UnsupportedOperativeSystemException {
		return getTerminal(OperativeSystemUtilities.getOs());
	}
}
