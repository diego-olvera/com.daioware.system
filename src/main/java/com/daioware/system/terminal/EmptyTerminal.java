package com.daioware.system.terminal;

import com.daioware.system.os.OperativeSystem;

public class EmptyTerminal extends Terminal{

	
	public EmptyTerminal(OperativeSystem os) {
		super(os);
	}

	@Override
	protected String getCommandLineExecutor() {
		return "";
	}

}
