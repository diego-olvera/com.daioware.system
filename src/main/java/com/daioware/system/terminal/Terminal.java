package com.daioware.system.terminal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.daioware.system.os.OperativeSystem;

public abstract class Terminal {
	
	private OperativeSystem os;
	private String lineBreak;
	
	public Terminal(OperativeSystem os) {
		this(os,"\r\n");
	}
	public Terminal(OperativeSystem os,String lineBreak) {
		setOs(os);
		setLineBreak(lineBreak);
	}

	public String getLineBreak() {
		return lineBreak;
	}

	public void setLineBreak(String lineBreak) {
		this.lineBreak = lineBreak;
	}

	public OperativeSystem getOs() {
		return os;
	}

	public void setOs(OperativeSystem os) {
		this.os = os;
	}

	public String execute(String command) throws IOException, InterruptedException{
		return execute(command,true);
	}
	public String execute(String command,boolean useCommandLineExecutor) throws IOException, InterruptedException{
		return execute(command,useCommandLineExecutor,true);
	}
	public String execute(String command,boolean useCommandLineExecutor,boolean returnCommandOutput) throws IOException, InterruptedException{
		StringBuffer output;
		BufferedReader reader=null;
		Process p;
		String line,linebreak=getLineBreak();
		try {
			p = Runtime.getRuntime().exec((useCommandLineExecutor?getCommandLineExecutor():"")+" "+command);
			p.waitFor();
			if(!returnCommandOutput) {
				return null;
			}
			output= new StringBuffer();
			reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = reader.readLine())!= null) {
				output.append(line).append(linebreak);
			}
		} finally{
			if(reader!=null){
				reader.close();
			}
		}
		return output.toString();
	}
	private void exec(String command) throws IOException {
		Runtime.getRuntime().exec(command);
	}
	public void executeInBackground(String command,boolean useCommandLineExecutor) throws IOException, InterruptedException{
		exec((useCommandLineExecutor?getCommandLineExecutor():"")+" "+command);
	}
	
	protected abstract String getCommandLineExecutor();

}
