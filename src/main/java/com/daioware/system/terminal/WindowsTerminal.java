package com.daioware.system.terminal;

import com.daioware.system.os.OperativeSystem;

public class WindowsTerminal extends Terminal{

	public WindowsTerminal(OperativeSystem os) {
		super(os);
	}

	@Override
	protected String getCommandLineExecutor() {
		return "cmd.exe /c";
	}
	
}
