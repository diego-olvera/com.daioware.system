package com.daioware.system.terminal;

import com.daioware.system.os.OperativeSystem;

public class UnixTerminal extends Terminal{

	public UnixTerminal(OperativeSystem os) {
		super(os,"\r\n");
	}

	@Override
	protected String getCommandLineExecutor() {
		return "sh -c";
	}
	
}
